---
title: Database Lab pricing
sidebar_label: Pricing
---

## Overview

- For the Enterprise Edition our billing is based on overall size of your databases;
- Billing is hourly based, $2.6624 / per TiB per hour (with GiB precision);
- Two weeks trial, that could be extended by participating in the Liable Beta Tester program;
- Database Lab Engine is always hoster on your infrastructure, we don't have an access to your data.

[Try Database Lab now](https://postgres.ai/console)

## Features

| Features | Community<br>Edition | Enterprise<br>Edition |
| :------- | :------------------: | :-------------------: |
| **Features** | **Community<br>Edition** | **Enterprise<br>Edition** |
|Price|Free|$2.6624 /<br>per TiB per hour|
||||
|**Platform – centralized GUI for management of all components**|❌|✅|
||||
|**Thin clones (Database Lab)**|||
|Thin cloning in seconds|✅|✅|
|Multiple snapshots and simultaneous independent clones|✅|✅|
|Full API access (REST, CLI, SDK)|✅|✅|
|Snapshot management and recycling|✅|✅|
|Clone management and recycling (of idle clones)|✅|✅|
|Customization of Postgres Docker images|✅|✅|
|Database refresh (from backup system, dumps): periodical or continuous|✅|✅|
|AWS RDS support|✅|✅|
|Anonymization / obfuscation / masking of PII data *(Roadmap)*|✅|✅|
|Simple deployment to AWS or GCP *(Roadmap)*|❌|✅|
|Fast recovery to arbitrary point in time (PITR) *(Roadmap)*|❌|✅|
||||
|**SQL optimization environment (Joe Bot)**|||
|Session management and recycling|✅|✅|
|Support hypothetical indexes|✅|✅|
|Support hypothetical partitioning *(Roadmap)*|✅|✅|
|Index advisor *(Roadmap)*|✅|✅|
|Slack chatbot|✅|✅|
|Web chatbot|❌|✅|
|Private chats|❌|✅|
|SQL optimization knowledge base|❌|✅|
|SQL query visualization|❌|✅|
|Integration with postgres-checkup, seamless SQL optimization workflow *(Roadmap)*|❌|✅|
|Advanced SQL tutorials *(Roadmap)*|❌|✅|
|Smart auto-deletion and/or auto-deletion control *(Roadmap)*|❌|✅|
|Channel mapping: one Joe can work with multiple Database Lab instances / multiple databases|❌|✅|
|Share session *(Roadmap)*|❌|✅|
||||
|**Healthcheck (postgres-checkup)**|||
|Reports and recommendations|✅|✅|
|Deep SQL performance analysis|✅|✅|
|Centralized storage of all reports|❌|✅|
|Performance and problems trend analysis *(Roadmap)*|❌|✅|
||||
|**BI**||
|Run long-running queries not affecting production health|✅|✅|
|Named queries / query organizer|❌|✅|
||||
|**Security and compliance**|||
|Login via: Google, LinkedIn, GitHub, GitLab|✅|✅|
|Invite team members|✅|✅|
|Integration with project management systems (Jira, GitHub Issues, GitLab Issues, etc) *(Roadmap)*|❌|✅|
|Automated sign-in using corporate email domain|❌|✅|
|SSO (Okta, ActiveDirectory) *(Roadmap)*|❌|✅|
|Audit log *(Roadmap)*|❌|✅|
|Security dashboard *(Roadmap)*|❌|✅|
|Roles / Team management / Personal tokens|❌|✅|
|Quotas management|❌|✅|
||||
|**Support and deployment**|||
|Community support|✅|✅|
|Guaranteed priority support|❌|✅|

[Try Database Lab now](https://postgres.ai/console)
