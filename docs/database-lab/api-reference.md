---
title: Database Lab API reference
sidebar_label: API reference
---

See [Database Lab Swagger](https://postgres.ai/swagger-ui/dblab/).
