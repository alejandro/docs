---
title: Database Lab guides
---

## Datbase Lab / Postgres.ai Platform
- [Start using Postgres.ai Platform](/docs/guides/platform/start-using-platform)

## How to work with Database Lab clones
- [How to create Database Lab clones](/docs/guides/cloning/create-clone)
- [How to connect to Database Lab clones](/docs/guides/cloning/connect-clone)
- [How to reset Database Lab clone](/docs/guides/cloning/reset-clone)
- [How to destroy Database Lab clone](/docs/guides/cloning/destroy-clone)
- [Protect clones from manual and automatic deletion](/docs/guides/cloning/clone-protection)

## Database Lab CLI
- [CLI install and init](/docs/guides/cli/cli-install-init)

## Administration
- [How to manage Database Lab Engine](/docs/guides/administration/engine-manage)
- [How to manage Joe Bot](/docs/guides/administration/manage-joe)
- [Secure Database Lab Engine](/docs/guides/administration/engine-secure)
- [Set up machine for Database Lab Engine](/docs/guides/administration/machine-setup)

## Obtaining data for Database Lab
### Logical retrieval
- [Amazon RDS](/docs/guides/data/rds)
- [Any database (dump/restore)](/docs/guides/data/dump)

### Physical retrieval
- [pg_basebackup](/docs/guides/data/pg_basebackup)
- [WAL-G](/docs/guides/data/wal-g)
- [Custom](/docs/guides/data/custom)
