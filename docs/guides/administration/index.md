---
title: Database Lab administration
sidebar_label: Administration
---

- [How to manage Database Lab Engine](/docs/guides/administration/engine-manage)
- [How to manage Joe Bot](/docs/guides/administration/manage-joe)
- [Secure Database Lab Engine](/docs/guides/administration/engine-secure)
- [Set up machine for Database Lab Engine](/docs/guides/administration/machine-setup)

[↵ Back to Guides](/docs/guides/)
