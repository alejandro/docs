---
title: How to work with Database Lab CLI
sidebar_label: Database Lab CLI
---

- [How to install and initialize Database Lab CLI](/docs/guides/cli/cli-install-init)

[↵ Back to Guides](/docs/guides/)
