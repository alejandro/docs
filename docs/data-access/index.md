---
title: Overview of data access use case
sidebar_label: Data access
---

Better performance for analytics

- Run heavy analytical SQL, perform data export without affecting the production servers.
- Analysts work with thin clones, which are fully independent.
- When a long-lasting query needs to be executed, an analyst can work independently, not interfering with production workload or a colleague's work.
- Production servers are not in danger: autovacuum activity is not affected, long-running queries are not causing bloat.
