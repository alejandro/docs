---
id: get-started
title: Getting started with Database Lab
hide_title: false
sidebar_label: Getting started
---

|   |  |
| ----------- | ----------- |
| [Database Lab Engine](/docs/database-lab)<br>Open-source technology to clone databases of any size in seconds | [Joe, SQL optimization chatbot](/docs/joe-bot)<br>Run `EXPLAIN ANALYZE` and optimize SQL on instantly provisioned full-size database copies |
| [Dev/QA/Staging databases with superpowers](/docs/staging)<br>Develop and test using full-size database copies provisioned in seconds | [CI/CD observer for DB schema changes](/docs/database-changes-cicd)<br>Prevent performance degrataion and downtime when deploying database schema changes | 
| [postgres-checkup](/docs/checkup)<br>Automated health-checks and query analsysis for heavily-loaded PostgreSQL databases | [Detached replicas](/docs/data-access)<br>Use BI tools, run analytical queries, perform data export without replication lags and bloat |

<!--#### [Data recovery /  Instantaneously recover lost data](/docs/data-recovery)
Recover accidentally deleted data. Using thin cloning, the point-in-time recovery (PITR) can be performed without long waiting.
-->

## What is Database Lab?

The Postgres.ai Platform aims to eliminate all the database-related roadblocks on the way of developers, DBAs, and QA engineers. We provide a technology to instantly provision copies of Postgres databases, and applications on top of it to help you manage, verify, diagnose and develop your databases.

![Database Lab architecture](/docs/assets/architecture.png)

- Full-sized database clones provisioned in seconds for various use case;
- One engine, endless applications;
- Simple to deploy and manage;
- Reduce infrastructure costs;
- Speed up you development and testing;
- Database diagnostics.

![CI/CD transformation with Database Lab](/docs/assets/cicd-transform.png)
